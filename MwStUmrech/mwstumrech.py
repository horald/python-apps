#*****************************************************
# Author...: Horst Meyer
# Date.....: 2020-06-20
# Source...: https://gitlab.com/horald/python-apps
# Copyright: (2020) GNU General Public License (GPLv2)
#*****************************************************

import mwstinit
from flask import Flask, session, redirect, url_for, escape, request
from flask_bootstrap import Bootstrap
app = Flask(__name__)
Bootstrap(app)

@app.route('/')
def start():
    html='Zum Starten bitte den nachfolgenden Link aufrufen:<br>'
    html=html+'<a href="/mwstabfrage">Mehrwersteuerumrechnung</a>'
    return html	

@app.route('/mwstumrech', methods=['GET', 'POST'])
def mwstumrech():
    html='mwstumrech:'
    if request.method == 'POST':
       mwsttypA=request.form['mwsttypA']    	
       mwsttypB=request.form['mwsttypB']
       waehrung='Euro'    	
       gpreisA=request.form['preisA']
       gpreisA=gpreisA.replace(',','.')
       gpreisB=request.form['preisB']
       gpreisB=gpreisB.replace(',','.')
       preisA=float(gpreisA)
       preisB=float(gpreisB)
       vonmwstsatz1=request.form['vonmwstsatz1']
       nchmwstsatz1=request.form['nchmwstsatz1']
       vonmwstsatz2=request.form['vonmwstsatz2']
       nchmwstsatz2=request.form['nchmwstsatz2']
       if (mwsttypA=='var1'):
          vonmwstsatzA=vonmwstsatz1
          nchmwstsatzA=nchmwstsatz1
       if (mwsttypA=='var2'):
          vonmwstsatzA=vonmwstsatz2
          nchmwstsatzA=nchmwstsatz2
       if (mwsttypA=='var3'):
          vonmwstsatzA=nchmwstsatz1
          nchmwstsatzA=vonmwstsatz1
       if (mwsttypA=='var4'):
          vonmwstsatzA=nchmwstsatz2
          nchmwstsatzA=vonmwstsatz2
       if (mwsttypB=='var1'):
          vonmwstsatzB=vonmwstsatz1
          nchmwstsatzB=nchmwstsatz1
       if (mwsttypB=='var2'):
          vonmwstsatzB=vonmwstsatz2
          nchmwstsatzB=nchmwstsatz2
       if (mwsttypB=='var3'):
          vonmwstsatzB=nchmwstsatz1
          nchmwstsatzB=vonmwstsatz1
       if (mwsttypB=='var4'):
          vonmwstsatzB=nchmwstsatz2
          nchmwstsatzB=vonmwstsatz2
       html='<table>'
       nettopreisA=float(preisA)*100/(100+float(vonmwstsatzA))
       mwstA=nettopreisA*float(vonmwstsatzA)/100
       neuermwstA=nettopreisA*float(nchmwstsatzA)/100
       neuerpreisA=neuermwstA+nettopreisA
       ersparnisA=preisA-neuerpreisA

       nettopreisB=float(preisB)*100/(100+float(vonmwstsatzB))
       mwstB=nettopreisB*float(vonmwstsatzB)/100
       neuermwstB=nettopreisB*float(nchmwstsatzB)/100
       neuerpreisB=neuermwstB+nettopreisB
       ersparnisB=preisB-neuerpreisB

       gesamtpreis=preisA+preisB
       gesamtmwst=mwstA+mwstB
       gesamtnettopreis=nettopreisA+nettopreisB
       gesamtneuermwst=neuermwstA+neuermwstB
       gesamtneuerpreis=neuerpreisA+neuerpreisB
       gesamtersparnis=ersparnisA+ersparnisB

       spreisA="%5.2f"% (preisA)
       smwstA="%5.2f"% (mwstA)
       snettopreisA="%5.2f"% (nettopreisA)
       sneuermwstA="%5.2f"% (neuermwstA)
       sneuerpreisA="%5.2f"% (neuerpreisA)
       sersparnisA="%5.2f"% (ersparnisA)

       spreisB="%5.2f"% (preisB)
       smwstB="%5.2f"% (mwstB)
       snettopreisB="%5.2f"% (nettopreisB)
       sneuermwstB="%5.2f"% (neuermwstB)
       sneuerpreisB="%5.2f"% (neuerpreisB)
       sersparnisB="%5.2f"% (ersparnisB)

       sgesamtpreis="%5.2f"% (gesamtpreis)
       sgesamtmwst="%5.2f"% (gesamtmwst)
       sgesamtnettopreis="%5.2f"% (gesamtnettopreis)
       sgesamtneuermwst="%5.2f"% (gesamtneuermwst)
       sgesamtneuerpreis="%5.2f"% (gesamtneuerpreis)
       sgesamtersparnis="%5.2f"% (gesamtersparnis)
       if (preisA!=0):
          html=html+'<tr><td>(A) Umrechnung:</td><td>von '+vonmwstsatzA+'% nach '+nchmwstsatzA+'%</td></tr>'
          html=html+'<tr><td>(A) Alter Preis:</td><td style="text-align:right">'+spreisA+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) <i>'+vonmwstsatzA+'% MwSt.:</i></td><td style="text-align:right">'+smwstA+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) Netto-Preis:</td><td style="text-align:right">'+snettopreisA+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) <i>'+nchmwstsatzA+'% MwSt.:</i></td><td style="text-align:right">'+sneuermwstA+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) Neuer Preis:</td><td style="text-align:right">'+sneuerpreisA+' '+waehrung+'</td></tr>'
          html=html+'<tr><td><u>(A) Ersparnis:</u></td><td style="text-align:right"><u>'+sersparnisA+' '+waehrung+'</u></td></tr>'
       if (preisB!=0):
          html=html+'<tr><td>(A) Umrechnung:</td><td>von '+vonmwstsatzB+'% nach '+nchmwstsatzB+'%</td></tr>'
          html=html+'<tr><td>(A) Alter Preis:</td><td style="text-align:right">'+spreisB+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) <i>'+vonmwstsatzB+'% MwSt.:</i></td><td style="text-align:right">'+smwstB+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) Netto-Preis:</td><td style="text-align:right">'+snettopreisB+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) <i>'+nchmwstsatzB+'% MwSt.:</i></td><td style="text-align:right">'+sneuermwstB+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>(A) Neuer Preis:</td><td style="text-align:right">'+sneuerpreisB+' '+waehrung+'</td></tr>'
          html=html+'<tr><td><u>(A) Ersparnis:</u></td><td style="text-align:right"><u>'+sersparnisB+' '+waehrung+'</u></td></tr>'
       if (preisA!=0 and preisB!=0):
          html=html+'<tr><td>Gesamter alter Preis:</td><td style="text-align:right">'+sgesamtpreis+' '+waehrung+'</td></tr>'
          html=html+'<tr><td><i>Gesamt-MwSt. alt:</i></td><td style="text-align:right">'+sgesamtmwst+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>Gesamt-Netto-Preis:</td><td style="text-align:right">'+sgesamtnettopreis+' '+waehrung+'</td></tr>'
          html=html+'<tr><td><i>Gesamt-MwSt. neu:</i></td><td style="text-align:right">'+sgesamtneuermwst+' '+waehrung+'</td></tr>'
          html=html+'<tr><td>Gesamter neuer Preis:</td><td style="text-align:right">'+sgesamtneuerpreis+' '+waehrung+'</td></tr>'
          html=html+'<tr><td><b>Gesamtersparnis:<b></td><td style="text-align:right"><b>'+sgesamtersparnis+' '+waehrung+'</b></td></tr>'
    	 
       html=html+'</table>'
       html=html+'<a href="/mwstabfrage">Neue MwSt-Umrechnung</a>'	
    else:
       html='Keine Umrechnung erkannt!'    
    return html

@app.route('/mwstabfrage')
def mwstabfrage():

    vonmwstsatz1=mwstinit.gvonmwstsatz1
    nchmwstsatz1=mwstinit.gnchmwstsatz1
    vonmwstsatz2=mwstinit.gvonmwstsatz2
    nchmwstsatz2=mwstinit.gnchmwstsatz2
    preisA='0' 
    preisB='0'
    html='<form class="form-horizontal" method="post" action="/mwstumrech">'
    html=html+'          <div class="control-group">'
    html=html+'            <label class="control-label" style="text-align:left" for="input01">Umrechnungssatz (A)</label>'
    html=html+'            <div class="input">'
    html=html+'<select id="input01" name="mwsttypA" size="1">';
    html=html+'<option style="background-color:#c0c0c0;" value="var1" selected>von '+vonmwstsatz1+'% zu '+nchmwstsatz1+'%</option>'
    html=html+'<option style="background-color:#c0c0c0;" value="var2" >von '+vonmwstsatz2+'% zu '+nchmwstsatz2+'%</option>'
    html=html+'<option style="background-color:#c0c0c0;" value="var3" >von '+nchmwstsatz1+'% zu '+vonmwstsatz1+'%</option>'
    html=html+'<option style="background-color:#c0c0c0;" value="var4" >von '+nchmwstsatz2+'% zu '+vonmwstsatz2+'%</option>'
    html=html+'</select>'
    html=html+'          </div>'
    html=html+'          </div>'
    html=html+'          <div class="control-group">'
    html=html+'            <label class="control-label" style="text-align:left" for="input02">Umrechnungssatz (B)</label>'
    html=html+'            <div class="input">'
    html=html+'<select id="input02" name="mwsttypB" size="1">'
    html=html+'<option style="background-color:#c0c0c0;" value="var1" >von '+vonmwstsatz1+'% zu '+nchmwstsatz1+'%</option>'
    html=html+'<option style="background-color:#c0c0c0;" value="var2" selected>von '+vonmwstsatz2+'% zu '+nchmwstsatz2+'%</option>'
    html=html+'<option style="background-color:#c0c0c0;" value="var3" >von '+nchmwstsatz1+'% zu '+vonmwstsatz1+'%</option>'
    html=html+'<option style="background-color:#c0c0c0;" value="var4" >von '+nchmwstsatz2+'% zu '+vonmwstsatz2+'%</option>'
    html=html+'</select>'
    html=html+'          </div>'
    html=html+'          </div>'
  
    html=html+'          <div class="control-group">'
    html=html+'            <label class="control-label" style="text-align:left" for="input03">Alter Preis (A)</label>'
    html=html+'            <div class="input">'
    html=html+'              <input type="text" id="input03" name="preisA" value="'+preisA+'">'
    html=html+'            </div>'
    html=html+'          </div>'
    html=html+'          <div class="control-group">'
    html=html+'            <label class="control-label" style="text-align:left" for="input04">Alter Preis (B)</label>'
    html=html+'            <div class="input">'
    html=html+'              <input type="text" id="input04" name="preisB" value="'+preisB+'">'
    html=html+'            </div>'
    html=html+'          </div>'
    html=html+'<input type="hidden" name="vonmwstsatz1" value="'+vonmwstsatz1+'">'
    html=html+'<input type="hidden" name="nchmwstsatz1" value="'+nchmwstsatz1+'">'
    html=html+'<input type="hidden" name="vonmwstsatz2" value="'+vonmwstsatz2+'">'
    html=html+'<input type="hidden" name="nchmwstsatz2" value="'+nchmwstsatz2+'">'

    html=html+'  <div class="form-actions">'
    html=html+'     <button type="submit" name="submit" class="btn btn-primary">Umrechnen</button>'
    html=html+'  </div>'
	 
    html=html+'</form>'
    return html