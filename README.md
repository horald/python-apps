# python-apps

Hier gibt es eine Sammlung kleiner Python-Apps.

## MwSt-Umrechnung
Bei der MwSt-Umrechnung handelt es sich um ein kleines Umrechnungsprogramm, dass zwischen zwei unterschiedlichen MwSt-Sätzen umrechnet und den daraus ergebenen Gewinn ausweist.